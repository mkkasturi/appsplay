package com.wavelabs

/**
  * Created by muralikrishnak on 11-Oct-17.
  */

import kafka.serializer.StringDecoder
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.InputDStream
import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.Date

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{ConnectionFactory, Put}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.{CellUtil, HBaseConfiguration, TableName}
import org.apache.hadoop.conf.Configuration

import scala.collection.mutable
import scala.util.parsing.json._

object HdfsStorage {

    def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("HDFS_Storage").setMaster("yarn-client")
    //Create Streaming context object
    val streamingContext = new StreamingContext(sparkConf, Seconds(20))

    //Kafka Topic name to which consumer is listening
    val topicsSet = Set("appsplay2000")

    val kafkaParams = Map[String, String]("metadata.broker.list" -> "rm01.itversity.com:6667,nn02.itversity.com:6667,nn01.itversity.com:6667")
    val now = Calendar.getInstance().getTime()
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
    val parentDirectory = dateFormat.format(new Date())
    val messages: InputDStream[(String, String)] = KafkaUtils.
      createDirectStream[String, String, StringDecoder, StringDecoder](
      streamingContext, kafkaParams, topicsSet)
    messages.saveAsTextFiles("hdfs://nn01.itversity.com:8020/user/mkkasturi11146/"+ parentDirectory + "/devices"+now.toString.replaceAll("\\s", "").replaceAll(":", ""))
    messages.print
    //Create Hbase Configuration Object
    val hBaseConf: Configuration = HBaseConfiguration.create()
    //Establish Connection
    val connection = ConnectionFactory.createConnection(hBaseConf)
    //Generate Campaign to Insert in Hbase
    val deviceCampaign = connection.getTable(TableName.valueOf("campaign_device_map"))
    val deviceCampaigns = messages.mapPartitions(devices => {
        // Write logic to get data from HBase and create HashMap
        val campaignData = List(
            AppsPlay("1", "India", "Telangana", "Hyderabad", "BSNL", "Canvas", "Active"),
            AppsPlay("2", "India", "Telangana", "Hyderabad", "Idea", "Silver", "Active")
        )
      devices.map(device => {
          var columnFamily1 = "eligibility"
          var columnName11 = "campaign_id"
          val json:Option[Any] = JSON.parseFull(device._2)
          val model = json.get.asInstanceOf[Map[String, Any]].get("model").get.asInstanceOf[String]
          campaignData.map((campaign : AppsPlay) => (
              if(campaign.deviceModel == model){
               /* val uuid = new Put(Bytes.toBytes(json.get.asInstanceOf[Map[String, Any]].get("imei").get.asInstanceOf[String]))
                uuid.addColumn(Bytes.toBytes(columnFamily1),Bytes.toBytes(columnName11),Bytes.toBytes(campaign.campaignId))
                deviceCampaign.put(uuid)*/
              }

          ))
          })
    })

    deviceCampaigns.saveAsTextFiles("hdfs://nn01.itversity.com:8020/user/mkkasturi11146/json")
    streamingContext.start()
    streamingContext.awaitTermination()
  }
}
