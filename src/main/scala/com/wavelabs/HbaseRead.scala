package com.wavelabs
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.{CellUtil, HBaseConfiguration, TableName}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp
import org.apache.hadoop.hbase.filter.{Filter, FilterList, SingleColumnValueFilter}

import scala.collection.mutable.ListBuffer


/**
  * Created by muralikrishnak on 13-Oct-17.
  */

case class AppsPlay(
                   campaignId: String,
                   country: String,
                   state: String,
                   city: String,
                   carrier: String,
                   deviceModel: String,
                   status: String
                   )

object HbaseRead {
  //Hbase Metadata
  var columnFamily1 = "target"
  var columnFamily2 = "metadata"
  var columnName11 = "country"
  var columnName12 = "state"
  var columnName13 = "city"
  var columnName14 = "carrier"
  var columnName15 = "device_model"
  var columnName21 = "status"

  /*
  * Write Processing logic for campaign data here.
  * Accepts Result object as parameter
  * */
  def processCampaignData(resultScanner:ResultScanner) = {
    var result = resultScanner.next
    val campaignData = ListBuffer[AppsPlay]();
    while(result != null) {
      val campaign = AppsPlay(Bytes.toString(result.getRow),
        Bytes.toString(result.getValue(Bytes.toBytes(columnFamily1), Bytes.toBytes(columnName11))),
        Bytes.toString(result.getValue(Bytes.toBytes(columnFamily1), Bytes.toBytes(columnName12))),
        Bytes.toString(result.getValue(Bytes.toBytes(columnFamily1), Bytes.toBytes(columnName13))),
        Bytes.toString(result.getValue(Bytes.toBytes(columnFamily1), Bytes.toBytes(columnName14))),
        Bytes.toString(result.getValue(Bytes.toBytes(columnFamily1), Bytes.toBytes(columnName15))),
        Bytes.toString(result.getValue(Bytes.toBytes(columnFamily2), Bytes.toBytes(columnName21))))
      campaignData += campaign
      result = resultScanner.next
    }
    campaignData.toList
  }

  def main(args: Array[String]): Unit ={
    //Create Hbase Configuration Object
    val hBaseConf: Configuration = HBaseConfiguration.create()
    //Establish Connection
    val connection = ConnectionFactory.createConnection(hBaseConf)
    val appsplay = connection.getTable(TableName.valueOf("appsplay"))
    //Create Filter
    val filters = new FilterList()
    val activeFilter = new SingleColumnValueFilter(Bytes.toBytes(columnFamily2),Bytes.toBytes(columnName21),CompareOp.EQUAL,Bytes.toBytes("ACTIVE"))
    filters.addFilter(activeFilter)
    //Set Filters to Query && Run Query on Table
    val scan = new Scan().setFilter(filters)
    val resultScanner = appsplay.getScanner(scan)
    //Iteration on Result Set
    val campaignData = processCampaignData(resultScanner)
    campaignData.foreach(println)
    campaignData.foreach((campaign : AppsPlay) => println(campaign.deviceModel))
  }
}
